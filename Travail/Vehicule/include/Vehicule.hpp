#pragma once
#include <string>
#include "../../Taille/include/Taille.hpp"
#include "../../Bonus/include/Bonus.hpp"

class Vehicule {
public:

private:
    std::string nom;
    float vitesse_max;
    float inertie;
    float maniabilite;
    Taille dimension;
    bool vulnerabilite;
    Bonus objet_bonus;
};
