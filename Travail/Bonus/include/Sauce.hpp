#pragma once
#include "../../Bonus/include/BonusPosition.hpp"

struct Point3D;

class Sauce : public BonusPosition {
public:

private:
    Point3D position_map;
    float duree;
    float vitesse_vehicule;
    float maniabilite_vehicule;
};
