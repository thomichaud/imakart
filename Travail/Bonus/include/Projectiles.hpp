#pragma once
#include "Bonus.hpp"
#include "../../Taille/include/Taille.hpp"

class Projectiles : public Bonus {
public:

private:
    float vitesse_vehicule_percute;
    float vitesse_projectile;
    Taille dimension;

};
